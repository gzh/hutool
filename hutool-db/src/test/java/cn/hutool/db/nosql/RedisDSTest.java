package cn.hutool.db.nosql;

import cn.hutool.db.nosql.redis.RedisDS;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import redis.clients.jedis.Jedis;

@Slf4j
public class RedisDSTest {

	@Test
//	@Ignore
	public void redisDSTest(){
		final Jedis jedis = RedisDS.create().getJedis();
		jedis.set("name","gzh");
		String v = jedis.get("name");

		log.info("v={}",v);
	}
}
